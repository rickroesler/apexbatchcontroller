trigger BatchCompletionTrigger on Batch_Completion__e (after insert) {

    //system.debug('in trigger');
    if (Trigger.isAfter && Trigger.isInsert) {
        BatchCompletionTriggerHandler.afterInsert(Trigger.new);
    }

}