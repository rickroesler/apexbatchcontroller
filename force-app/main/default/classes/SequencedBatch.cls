public abstract class SequencedBatch {
    
    // Each batch in the sequence receiveds a "state" that provides information on what it's to do.
    //
    // Each batch updates the "state" when it's done and must call the publishCompletionEvent() method
    // as the last statement of the batch's finish() method.

    protected BatchSequencer.SequenceState state;
    
    // We're using the JSON deserialization pattern to instantiate these batch classes
    // so that we can pass in (a JSON-serialized) list of parameters.
    //
    // NOTE: unlike Java, Apex only invokes the no-parameter constructor when you use Type.newInstance() 
    //       to instantiate a class instance; you can't pass parameters using Type.newInstance()
    //
    // The pattern is outlined by Zach Elrath in this Stackexchange answer:
    //      https://salesforce.stackexchange.com/a/6023/64372
    //
    // Upon deserialization of the class, the setter for each class parameter is passed an implicit "value".
    // 
    // Here, we've assumed that the batch needs only one parameter, the BatchSequencer.SequenceState state, 
    // and that it can get everything it needs to run from that state. We're using the jsonState parameter 
    // to receive a JSON-serialized representation of this state.
    //
    // NOTE: jsonState is write-only, and we never reference it anywhere else, we're just using the "value" 
    //       that is passed in to set the state for this batch. We don't actually set the jsonState parameter.
    //       The state is set as a side-effect of the Setter.
    // NOTE: The setter has to be coded in this way. Creating a separate public void setJsonState(String value) 
    //       method does not work. I don't understand why.
    private String jsonState {
       private set {
           //system.debug('Batch0: jsonState: ' + value);
           this.state = (BatchSequencer.SequenceState) JSON.deserialize(value, BatchSequencer.SequenceState.class);
           //system.debug('Batch0: state: ' + state);
       } 
    }

    public void publishCompletionEvent(BatchSequencer.SequenceState state) {
        
        // update the sequence state to signal that this batch has completed
        // NOTE: this.toString() returns a string like "thisClassName:aBunchOfOtherStuff"
        String thisBatchName = this.toString().split(':')[0];
        state.setPreviousBatch(thisBatchName);
        
        List<Batch_Completion__e> batchCompletionEvents = new List<Batch_Completion__e>();
        batchCompletionEvents.add(new Batch_Completion__e(Sequence_State_Json__c=JSON.serialize(state)));

        // Call method to publish events
        List<Database.SaveResult> results = EventBus.publish(batchCompletionEvents);

        // Inspect publishing result for each event
        for (Database.SaveResult sr : results) {
            if (sr.isSuccess()) {
                System.debug('Successfully published event.');
            } else {
                for(Database.Error err : sr.getErrors()) {
                    System.debug('Error returned: ' +
                                err.getStatusCode() +
                                ' - ' +
                                err.getMessage());
                }
            }       
        }

    }
}
