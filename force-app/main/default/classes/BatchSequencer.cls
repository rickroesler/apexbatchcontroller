public with sharing class BatchSequencer {

    private static final Map<String, List<BatchSequencer.BatchDescriptor>> SEQUENCE;
    private BatchSequencer.SequenceState state;

    static {
        SEQUENCE = new Map<String, List<BatchSequencer.BatchDescriptor>>{
            null => new List<BatchSequencer.BatchDescriptor>{new BatchSequencer.BatchDescriptor('Batch0', 200)},
            'Batch0' => new List<BatchSequencer.BatchDescriptor>{new BatchSequencer.BatchDescriptor('Batch1')},
            'Batch1' => new List<BatchSequencer.BatchDescriptor>()};
    }

    public BatchSequencer(BatchSequencer.SequenceState state) {
        this.state = state;
        executeBatches();
    }

    public BatchSequencer(String jsonSequenceState) {
        this.state = (BatchSequencer.SequenceState) JSON.deserialize(jsonSequenceState, BatchSequencer.SequenceState.class);
        executeBatches();
    }

    private void executeBatches() {
        system.debug('BatchSequencer: continuation from: ' + this.state.getPreviousBatch());
        List<BatchSequencer.BatchDescriptor> nextBatches = SEQUENCE.get(this.state.getPreviousBatch());
        for (BatchSequencer.BatchDescriptor batch: nextBatches) {
            executeBatch(batch);
        }
    }


    private void executeBatch(BatchSequencer.BatchDescriptor batch) {

        // NOTE: in the following, it seems to not matter whether the Database.Batchable parameter
        //       (for example, "sObject") matches the actual parameter of the instantiated class 
        System.Type t = Type.forName(batch.batchName);
        
        // NOTE: we have to escape all the " in the JSON state serialization because we're passing
        //       a JSON string as an argument in another JSON string
        String jsonState = '{"jsonState":"' + JSON.serialize(this.state).replaceAll('"', '\\\\"') + '"}';
        
        Database.Batchable<sObject> initialBatchInstance = (Database.Batchable<sObject>) JSON.deserialize(jsonState,t);

        // Did the call succeed?
        boolean callSucceeded = initialBatchInstance != null;
        system.debug('callSucceeded = ' + callSucceeded);
        
        if (batch.batchSize == null) {
            Database.executeBatch(initialBatchInstance);
        } else {
            Database.executeBatch(initialBatchInstance, batch.batchSize);
        }
    }
    

    // Inner class that stores the name and the batch size of a SequencedBatch class instance
    class BatchDescriptor {
        String batchName;
        Integer batchSize;
        
        BatchDescriptor(String batchName, Integer batchSize) {
            this.batchName = batchName;
            this.batchSize = batchSize;
        }

        BatchDescriptor(String batchName) {
            this.batchName = batchName;
            this.batchSize = null;
        }
    }


    // Inner class that is used to pass parameters to the SequencedBatch classes and
    // keep track of their state.
    // The parameters 'sequenceId' and 'previousBatch' are required and are handled automatically; do not touch that logic.
    // 
    public class SequenceState {
        // sequenceId and previousBatch are required
        private String sequenceId;
        private String previousBatch;
        // add any other additional parameters here
        private String param1;
        private Set<String> param2;

        public SequenceState() {
            sequenceId = EncodingUtil.convertToHex(Crypto.generateAesKey(128));
        }

        // sequeceId is read-only
        public String getSequenceId() {
            return sequenceId;
        }

        public String getPreviousBatch() {
            return previousBatch;
        }

        public void setPreviousBatch(String batchName) {
            previousBatch = batchName;
        }

        // add getters and setters for additional parameters
        // ....
    }
}