public with sharing class Batch1 extends SequencedBatch implements Database.Batchable<String> {
    
    // when you create an instance via JSON deserialization, the constructor is never called
    public Batch1() {
        system.debug('Batch1: constructor');
    }

    public List<String> start(Database.BatchableContext bc) {
        system.debug('Batch1: start');
        system.debug('Batch1: state: ' + state);
        return new List<String>{'test1'};
    }

    public void execute(Database.BatchableContext bc, List<String> keys) {
        system.debug('Batch1: ' + keys[0]);
    }

    public void finish(Database.BatchableContext bc) {
        system.debug('Batch1: finish');

        // perform required cleanup operations
        // ....

        // update any other sequence state parameters
        // ....

        publishCompletionEvent(state);
    }
}
