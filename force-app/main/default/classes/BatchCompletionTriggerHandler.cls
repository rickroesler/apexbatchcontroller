public with sharing class BatchCompletionTriggerHandler {
    
    public static void afterInsert(List<Batch_Completion__e> events) {
        for (Batch_Completion__e event: events) {
            //system.debug('in afterInsert handler for loop');
            //system.debug(event);
            new BatchSequencer(event.Sequence_State_Json__c);
        }
    }

}
