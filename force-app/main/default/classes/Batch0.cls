public with sharing class Batch0 extends SequencedBatch implements Database.Batchable<String> {

    // when you create an instance via JSON deserialization, the constructor is never called
    public Batch0() {
        system.debug('Batch0: constructor');
    }

    public List<String> start(Database.BatchableContext bc) {
        system.debug('Batch0: start');
        system.debug('Batch0: state: ' + state);
        return new List<String>{'test0'};
    }

    public void execute(Database.BatchableContext bc, List<String> keys) {
        system.debug('Batch0: ' + keys[0]);
    }

    public void finish(Database.BatchableContext bc) {
        system.debug('Batch0: finish');

        // perform required cleanup operations
        // ....

        // update any other sequence state parameters
        // ....

        //BatchSequencer.publishCompletionEvent(JSON.serialize(state));
        publishCompletionEvent(state);
    }

}
